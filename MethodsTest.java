public class MethodsTest 
{
	public static void main(String[] args)
	{
		int x = 5;	
		System.out.println(x);
		methodNoInputNoOutput();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(x, 2.5);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		double rootSum = sumSquareRoot(9, 5);
		System.out.println(rootSum);
		String s1 = "java";
		String s2 = "programming";
		System.out.println("Length of " + s1 + ":");
		System.out.println(s1.length());
		System.out.println("Length of " + s2 + ":");
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));		
	}
	
	public static  void methodNoInputNoOutput()
	{
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int x)
	{
		x -= 5;
		System.out.println("Inside the method one input no return");
		System.out.println(x);
	}
	
	public static void methodTwoInputNoReturn(int x, double y)
	{
		System.out.println("Inside the method two input no return");
		System.out.println(x);
		System.out.println(y);
	}
	
	public static  int methodNoInputReturnInt()
	{
		return 5;
	}
	
	public static double sumSquareRoot(int x, int y)
	{
		double z = Math.sqrt(x+y);
		return z;
	}
}