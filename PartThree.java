import java.util.Scanner;
public class PartThree
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a number which will be added to the second number,");
		System.out.println("subtracted from the second number, divided by the second number,");
		System.out.println("and multiplied by the second number.");
		double x = scan.nextDouble();
		System.out.println("Enter a second number");
		double y = scan.nextDouble();
		
		System.out.println("Addition of " + x + " and " + y);
		System.out.println(Calculator.add(x, y));
		
		System.out.println("Subtraction of " + y + " by " + x);
		System.out.println(Calculator.subtract(x, y));
		
		Calculator calc = new Calculator();
		
		System.out.println("Multiplication of " + x + " and " + y);
		System.out.println(calc.multiply(x, y));
		
		System.out.println("Division of " + x + " by " + y);
		System.out.println(calc.divide(x, y));
		
		scan.close();
	}
}